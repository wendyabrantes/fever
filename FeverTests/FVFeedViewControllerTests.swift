//
//  FVFeedViewControllerTests.swift
//  Fever
//
//  Created by Wendy Abrantes on 22/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import XCTest
@testable import Ono
@testable import Fever

class FVFeedViewControllerTests: XCTestCase {
    
    var feedViewController : FVFeedViewController!
    var oldFeed : ONOXMLDocument?
    var newFeed : ONOXMLDocument?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        feedViewController = FVFeedViewController(viewModel: FVFeedViewControllerViewModel())
        feedViewController.loadView()
        
        UIApplication.sharedApplication().keyWindow?.rootViewController = feedViewController
        
        if let path = NSBundle(forClass: FVFeedViewControllerTests.self).pathForResource("old_feed", ofType: "xml") {
            do {
                let data = NSData(contentsOfFile:path)
                oldFeed = try ONOXMLDocument(data: data)
            } catch _ as NSError {
                
            }
        }
        
        if let path = NSBundle(forClass: FVFeedViewControllerTests.self).pathForResource("new_feed", ofType: "xml") {
            do {
                let data = NSData(contentsOfFile:path)
                newFeed = try ONOXMLDocument(data: data)
            } catch _ as NSError {
                
            }
        }
        
        XCTAssertNotNil(oldFeed)
        XCTAssertNotNil(newFeed)
        
        guard let _ = oldFeed!.rootElement.firstChildWithTag("channel") else{
            XCTFail()
            return
        }
        guard let _ = newFeed!.rootElement.firstChildWithTag("channel") else{
            XCTFail()
            return
        }
        
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testParseRSSFeed() {
        //feedViewController.alertLatestPublishedArticle()
        
        var countItemOldFeed = 0
        var countItemNewFeed = 0
        if let channel = oldFeed!.rootElement.firstChildWithTag("channel") {
            countItemOldFeed = channel.childrenWithTag("item")!.count
        }
        if let channel = newFeed!.rootElement.firstChildWithTag("channel") {
            countItemNewFeed = channel.childrenWithTag("item")!.count
        }
        
        XCTAssertEqual(feedViewController.collectionView(feedViewController.collectionView, numberOfItemsInSection: 0), 0)
        feedViewController.viewModel.parseRSSFeed(oldFeed!)
        XCTAssertEqual(feedViewController.collectionView(feedViewController.collectionView, numberOfItemsInSection: 0), countItemOldFeed)
        feedViewController.viewModel.parseRSSFeed(newFeed!)
        XCTAssertEqual(feedViewController.collectionView(feedViewController.collectionView, numberOfItemsInSection: 0), countItemNewFeed)

    }
    
    func testAlertLatestPublishedArticle() {
        
        XCTAssertNil(feedViewController.presentedViewController)
        
        feedViewController.viewModel.parseRSSFeed(oldFeed!)
        feedViewController.viewModel.showAlertView.value = true
        
        XCTAssertTrue(feedViewController.presentedViewController is UIAlertController)
        XCTAssertEqual(feedViewController.presentedViewController?.title, "Latest published article")
    }
    
    func testAlertLatestPublishedArticleFromViewModel(){
        feedViewController.viewModel.parseRSSFeed(oldFeed!)
        feedViewController.viewModel.parseRSSFeed(newFeed!)
     
        XCTAssertTrue(feedViewController.presentedViewController is UIAlertController)
        XCTAssertEqual(feedViewController.presentedViewController?.title, "Latest published article")
    }
    
    
    
}
