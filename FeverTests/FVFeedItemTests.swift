//
//  FVFeedItemTests.swift
//  Fever
//
//  Created by Wendy Abrantes on 22/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import XCTest
@testable import Fever

class FVFeedItemTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFeedItemModel() {
        
        let item1 = FVFeedItem(title: "title test", description: "description", publishedDate: "Mon, 07 Feb 2016 00:00:00 -0800", author: "wendy abrantes", articleLink: "http://testurl.co")
        
        XCTAssertEqual(item1.title, "title test")
        XCTAssertNotNil(item1.articleLink)
        XCTAssertNotNil(item1.publishedDate)
        
        let item2 = FVFeedItem(title: "title article", description: "description", publishedDate: "wrong date 3030 4351 -098", author: "wendy abrantes", articleLink: "http://testurl.co")
        XCTAssertNil(item2.publishedDate)
        
    }
}
