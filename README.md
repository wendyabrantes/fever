
FEVER swift test app

- impletemented MVVM architecture with RXSwift for binding
- used Alamofire for networking and ONO for XML parsing 
- for the unit test I would normally use OCMock in objective-c but it's not supported when writting the test in swift, so i've decided to just load dummy data from an xml files.
  