//
//  FVFeedViewControllerViewModel.swift
//  Fever
//
//  Created by Wendy Abrantes on 21/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import Ono

final class FVFeedViewControllerViewModel {

    let disposeBag = DisposeBag()
    
    private(set) var showAlertError : Variable<Bool> = Variable(false)
    private(set) var showAlertView : Variable<Bool> = Variable(false)
    private(set) var lastBuildDate : Variable<String> = Variable("")
    private(set) var feedCellViewModels = [FVFeedCellViewModel]()
    private var myTimer : NSTimer?
    
    func lastestPublishedItem() -> FVFeedCellViewModel? {
        return (feedCellViewModels.count > 0) ? feedCellViewModels[0] : nil
    }
    
    func feedCellViewModelAtIndex(index: Int) -> FVFeedCellViewModel {
        return feedCellViewModels[index]
    }
    
    func numberOfItems() -> Int {
        return feedCellViewModels.count
    }
    
    func parseRSSFeed(XML : ONOXMLDocument){
        if let channel = XML.rootElement.firstChildWithTag("channel") {
            let items = FVModelFactory.arrayOfFeedItemFromXML(channel.childrenWithTag("item"))
            self.feedCellViewModels = [FVFeedCellViewModel]()
            items.forEach {
                self.feedCellViewModels.append(FVFeedCellViewModel(feedItem: $0))
            }
            //updated_at
            let latestDate = channel.firstChildWithTag("pubDate").stringValue()
            
            if self.lastBuildDate.value != "" && latestDate != self.lastBuildDate.value {
                self.showAlertView.value = true
            }
            
            self.lastBuildDate.value = channel.firstChildWithTag("pubDate").stringValue()
            
        }
    }
    
    //MARK: POLLING 
    
    func startLongPolling() {
        stopLongPolling()
        
        myTimer = NSTimer(timeInterval: 10*60, target: self, selector: #selector(FVFeedViewControllerViewModel.refreshRSSFeed), userInfo: nil, repeats: true)
        myTimer?.fire()
        NSRunLoop.mainRunLoop().addTimer(myTimer!, forMode: NSDefaultRunLoopMode)
    }
    
    func stopLongPolling() {
        if let myTimer = myTimer where myTimer.valid {
            myTimer.invalidate()
        }
    }
    
    @objc func refreshRSSFeed() {
        Alamofire.request(.GET, "http://nshipster.com/feed.xml")
            .responseXMLDocument { [weak self] response in
                
                if response.result.isFailure {
                    self?.showAlertError.value = true
                }
                
                if response.result.isSuccess {
                    if let XML = response.result.value, let channel = XML.rootElement.firstChildWithTag("channel") {
                        let newPubDate = channel.firstChildWithTag("pubDate").stringValue()
                        if self?.lastBuildDate.value != newPubDate {
                            self?.parseRSSFeed(XML)
                        }
                    }else{
                        self?.showAlertError.value = true
                    }
                }
        }
    }
}

