//
//  FVFeedCellViewModel.swift
//  Fever
//
//  Created by Wendy Abrantes on 21/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

final class FVFeedCellViewModel : NSObject {
    
    private var feedItem : FVFeedItem!
    
    private(set) var title : Variable<String>!
    private(set) var htmlDescription : Variable<String>!
    private(set) var author : Variable<String>!
    private(set) var weblink : Variable<NSURL?>?
    
    init(feedItem: FVFeedItem){
        super.init()
        self.feedItem = feedItem
        bindModel()
    }
    
    // MARK: - Methods
    private func bindModel() {
        self.title = Variable(self.feedItem.title)
        self.htmlDescription =  Variable(self.feedItem.description)
        self.author = Variable(self.feedItem.author)
        self.weblink = Variable(self.feedItem.articleLink)
    }

}
