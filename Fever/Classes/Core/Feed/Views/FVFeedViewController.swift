//
//  FVFeedViewController.swift
//  Fever
//
//  Created by Wendy Abrantes on 21/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa
import Ono

class FVFeedViewController : UIViewController {
    
    // MARK: - Vars
    let disposeBag = DisposeBag()
    
    internal private(set) var viewModel: FVFeedViewControllerViewModel!
    internal let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())

    
    // MARK: - Constructors
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(viewModel: FVFeedViewControllerViewModel!) {
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
        bindViewModel()
    }
    
    // MARK: - Layout
    override func loadView() {
        super.loadView()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerClass(FVFeedCell.self, forCellWithReuseIdentifier: FVFeedCell.reuseIdentifier)
        collectionView.delaysContentTouches = false
        collectionView.showsHorizontalScrollIndicator = false
        view.addSubview(collectionView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "NSHipster Feed"
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.startLongPolling()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.stopLongPolling()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        collectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
    }
    
    // MARK: - Method

    func bindViewModel(){
        viewModel.lastBuildDate
            .asObservable()
            .subscribe { _ in
                self.collectionView.reloadData()
            }
            .addDisposableTo(disposeBag)
        
        viewModel.showAlertView
            .asObservable()
            .subscribeNext { value in
                if value {
                    self.alertLatestPublishedArticle()
                }
            }
            .addDisposableTo(disposeBag)
        
        viewModel.showAlertError
            .asObservable()
            .subscribeNext { value in
                if value && self.viewModel.numberOfItems() == 0 {
                    self.alertFeedUpdatedError()
                }
                
        }.addDisposableTo(disposeBag)
    }
    
    func alertFeedUpdatedError(){
        if self.presentedViewController == nil {
            let alertController = UIAlertController(title: "Fever error", message: "Sorry, We couldn't load the items", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "retry", style: UIAlertActionStyle.Default, handler: { alert in
               self.viewModel.refreshRSSFeed()
            }))
            
            presentViewController(alertController, animated: true, completion: nil)
        }
    }

    func alertLatestPublishedArticle(){
        let feedCellViewModel = viewModel.lastestPublishedItem()
        
        let alertController = UIAlertController(title: "Latest published article", message: feedCellViewModel?.title.value, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.Default, handler: nil))
        presentViewController(alertController, animated: true, completion: nil)
    }
}

// MARK: - UICollectionView Data Source

extension FVFeedViewController: UICollectionViewDataSource {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfItems()
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(FVFeedCell.reuseIdentifier, forIndexPath: indexPath) as! FVFeedCell
        cell.viewModel = viewModel.feedCellViewModelAtIndex(indexPath.row)
        return cell
    }   
}

// MARK: - UICollectionView Delegate Flow Layout

extension FVFeedViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width, height: 100.0)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10.0
    }
    
}