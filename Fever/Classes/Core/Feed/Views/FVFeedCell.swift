//
//  FVFeedCell.swift
//  Fever
//
//  Created by Wendy Abrantes on 21/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class FVFeedCell : UICollectionViewCell {
    
    // MARK: - Vars
    static var reuseIdentifier = "FVFeedCellIdentifier"
    
    let disposeBag = DisposeBag()
    
    private var titleLabel = UILabel()
    private var descriptionLabel = UILabel()
    private var authorLabel = UILabel()
    
    let horizontalContentInset: CGFloat = 14.0
    let verticalContentInset: CGFloat = 14.0
    
    var viewModel: FVFeedCellViewModel? {
        didSet {
            bindViewModel()
        }
    }
    
    // MARK: - Constructors
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.whiteColor()
        
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(authorLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Layout
    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.sizeToFit()
        authorLabel.sizeToFit()
        titleLabel.frame = CGRectMake(horizontalContentInset, verticalContentInset, bounds.width - (2*horizontalContentInset), titleLabel.frame.height)
        authorLabel.frame = CGRectMake(horizontalContentInset, titleLabel.frame.maxY + verticalContentInset, bounds.width - (2*horizontalContentInset), authorLabel.frame.height)
    }
    
    // MARK: - Methods
    func bindViewModel() {
        if let viewModel = viewModel {
            viewModel.title
                .asObservable()
                .subscribeNext({ value in
                    self.titleLabel.text = value
                })
                .addDisposableTo(disposeBag)
            viewModel.author
                .asObservable()
                .subscribeNext({ value in
                    self.authorLabel.text = value
                })
                .addDisposableTo(disposeBag)
        }
    }
}
