//
//  FVFeedItem.swift
//  Fever
//
//  Created by Wendy Abrantes on 21/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import Foundation

public struct FVFeedItem {

    // MARK: - Vars

    public private(set) var title: String
    public private(set) var description: String
    public private(set) var publishedDate: NSDate?
    public private(set) var author: String
    public private(set) var articleLink: NSURL?
    
    private let dateFormatter = NSDateFormatter()

    // MARK: - Constructors

    public init(title: String, description: String, publishedDate: String, author: String, articleLink: String) {
        
        dateFormatter.dateFormat = "EEE, DD MMM yyyy HH:mm:ss Z"

        self.title = title
        self.publishedDate = dateFormatter.dateFromString(publishedDate)
        self.description = description
        self.author = author
        self.articleLink = NSURL(string: articleLink)
    }
}
