//
//  FVModelFactory.swift
//  Fever
//
//  Created by Wendy Abrantes on 21/04/2016.
//  Copyright © 2016 MarvelApp. All rights reserved.
//

import Foundation
import Ono
public class FVModelFactory {
    
    // MARK: - Methods
    
    
    public class func arrayOfFeedItemFromXML(xmlItems: [AnyObject]) -> [FVFeedItem] {
        
        var models = [FVFeedItem]()
        
        xmlItems.forEach {
            let title = $0.firstChildWithTag("title").stringValue() ?? ""
            let description = $0.firstChildWithTag("description").stringValue() ?? ""
            let publishedDate = $0.firstChildWithTag("pubDate").stringValue() ?? ""
            var author = "unknown"
            if let authorNode = $0.firstChildWithTag("author") {
                author = authorNode.stringValue()
            }
            let articleLink = $0.firstChildWithTag("link").stringValue()
            
            let item = FVFeedItem(title: title, description: description, publishedDate: publishedDate, author: author, articleLink: articleLink)
            models.append(item)
        }
        
        return models
    }
    
}